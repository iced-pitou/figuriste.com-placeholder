[**<--**](blog.html?lang=en)

# Hollow, Keenness & Profile

---

Have you ever looked at your skate blades and wondered why they're shaped the way they are? In this post, we're looking at the secrets of blade geometry. Understanding the parameters of your blades will help you make informed decisions regarding purchase and maintenance.

The 3 aspects of blade anatomy are:
- Hollow
- Keenness
- Profile
---

### Hollow - The Skater's Choice

#### What is Hollow?

The goal of a blade is to provide orthogonal bite—allowing skaters to push off the ice at a right angle to the direction of glide. The hollow controls how much bite an edge has.

Hollow is commonly measured by a radius, either in millimeters or fractions of an inch.

#### How Does Hollow Impact Skating?

The ideal hollow gives just enough adhesion for propulsion and control. Too much bite makes stopping and gliding difficult, while too little causes drift when leaning on an edge.

#### Factors That Determine Your Ideal Hollow

1. **Skating Style**

   - Intense movements require a deeper hollow, while relaxed strides work better with a shallower hollow.
   - **Figure Skating:** Beginners typically start at 15mm (5/8"). As skills improve with jumps and flying spins, 12mm (1/2") or even 9mm (3/8") may be more suitable. Ice dancers may stay at 15mm unless using thinner dance blades.
   - **Hockey:** 12mm is a common starting point. Adjust based on position and preference. Goalies often use ~19mm (3/4").

2. **Ice Hardness**

   - **Hot ice** (above -5°C) is softer and dampens glide.
   - **Cold ice** (below -5°C) is hard and brittle, requiring more hollow for optimal bite.

3. **Blade Thickness**

   - The same hollow reacts differently on blades of varying thickness due to changes in bite angle.
   - Ice dance blades (~3mm) are thinner than freestyle blades (~4mm). Advanced dancers may use ~8mm hollows to account for reduced adherence.
   - Hockey blades are ~3mm thick, while goalie blades are ~4mm.

4. **Personal Preference**

   - Individual experience will ultimately determine the ideal hollow. Consider it a range, not a fixed value, and always evaluate on a freshly sharpened blade.

### Keenness - Essential Checkpoint

#### What is Keenness?

Keenness refers to the sharpness of the transition between the side of the blade and the hollow, forming an edge. Unlike hollow, keenness dulls during skating.

#### Sharpening and Keenness

    - Sharpening increases keenness by recreating a sharp edge.
    - Sharpening beyond the keenness point generates a burr, which must be removed. Traditional grinding stones may fold the burr into an "overburr," which creates excessive bite until worn down.
    - The **Ice Gold** tool cuts the burr cleanly to avoid this issue.

#### Protecting Keenness

Ice itself doesn’t dull edges—particulates in the ice do. Always wear rigid skate guards on rubber flooring to protect your edges.

### Profile - Key to Consistency

#### What is Profile?

The profile of a blade refers to its curvatures, called **rockers**, and the **sweet spot**. It determines how much of the blade contacts the ice and affects turning and maneuverability.

#### Rockers Explained

1. **Major Rocker**

   - A large arc with a specific radius (e.g., 2m–2.5m).

2. **Minor Rocker**

   - Tangential curves near the base of the drag pick, influencing turning ease.

#### Sweet Spot

The sweet spot is where the blade has minimal contact with the ice, making it ideal for turning.

#### Profile Maintenance

- **Machine Sharpening:** Can degrade the profile by creating peaks and valleys, especially near the drag pick.
- **Hand Sharpening:** Gentle cutting action preserves the profile and allows sharpening up to the base of the drag pick.

### Clarifications

In this post, the term **blade** refers to the entire skate runner. Sharpening concerns only the runner portion of the blade.

\*Scales in illustrations are exaggerated for clarity.

[**<--**](blog.html?lang=en)
